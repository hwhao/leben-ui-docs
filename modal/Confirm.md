# Confirm

> 基于Modal的确认窗口



### Props

* show `Boolean:false` 控制显示和隐藏
* title `String:'确定'` 窗口标题
* icon `String:'fa fa-info'` 窗口标题图标\
* confirmText `String:'确定'` 确定按钮文本
* confirmIcon `String:'fa fa-check'` 确定按钮图标
* cancelText `String:'取消'` 取消按钮文本
* cancelIcon `String:'fa fa-close'` 取消按钮图标
* maskClose `Boolean:true` 是否可通过点击遮罩关闭窗口



### Event

* on-confirm `fn()` 点击确定按钮时触发
* on-cancel `fn()` 点击取消按钮时触发



### Example

```html
<confirm :show="true">提示内容</confirm>
```
