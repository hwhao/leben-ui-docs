# Alert

> 基于Modal的警告窗口



### Props

* show `Boolean:false` 控制显示和隐藏
* title `String:'确定'` 窗口标题
* icon `String:'fa fa-info'` 窗口标题图标\
* confirmText `String:'确定'` 确定按钮文本
* confirmIcon `String:'fa fa-check'` 确定按钮图标
* confirmType `String:'primary'` 确定按钮类型，支持类型请查阅XButton.type
* maskClose `Boolean:true` 是否可通过点击遮罩关闭窗口



### Event

* on-confirm `fn()` 点击确定按钮时触发



### Example

```html
<alert :show="true">提示内容</alert>
```
