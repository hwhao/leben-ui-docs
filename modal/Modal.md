# Modal

> 带全屏遮罩的基础模态窗口，窗体内容完全自定义



### Props

* show `Boolean:false` 控制显示和隐藏
* borderless `Boolean:false` 是否无窗体边框，默认有圆角边框和白色背景，设置为true则完全自定义
* maskClose `Boolean:true` 是否可通过点击遮罩关闭窗口



### Event

* on-show `fn()` 窗口显示时触发
* on-hide `fn()` 窗口隐藏时触发



### Example

```html
<modal :show="true">
	<div>窗口内容</div>
</modal>
```
