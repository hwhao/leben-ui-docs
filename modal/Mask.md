# Mask

> 全屏遮罩层



### Props

* show  `Boolean:false` 控制显示和隐藏
* transition `String` 显示和隐藏的过渡动画



### Example

```html
<mask :show="true"></mask>
```
