# Summary

* [关于](README.md)


* [安装和使用](install.md)


* [布局组件]()
  * [SideMenu](layout/SideMenu.md)
  * [SidePanel](layout/SidePanel.md)
  * [Box](layout/Box.md)


* [模态窗口]()
  * [Mask](modal/Mask.md)
  * [Modal](modal/Modal.md)
  * [Alert](modal/Alert.md)
  * [Confirm](modal/Confirm.md)


* [表单组件]()
  * [XButton](form/XButton.md)
  * [XInput](form/XInput.md)
  * [Checkbox](form/Checkbox.md)
  * [Dropdown](form/Dropdown.md)
  * [Radio](form/Radio.md)
  * [ImgSelector](form/ImgSelector.md)
  * [ImgUploader](form/ImgUploader.md)
  * [SearchBar](form/SearchBar.md)


* [UI组件]()
  * [Avatar](ui_components/Avatar.md)
  * [Breadcrumb](ui_components/Breadcrumb.md)
  * [Loader](ui_components/Loader.md)
  * [Message](ui_components/Message.md)
  * [Popup](ui_components/Popup.md)
  * [Pagination](ui_components/Pagination.md)
  * [Tabs](ui_components/Tabs.md)
  * [XLabel](ui_components/XLabel.md)
