# Avatar

> 头像组件



### Props

* src `String:''` 头像图片地址
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`
* icon `String:''` 图标class
* radius `String:'64px'` 圆角大小
* bgColor `String:'transparent'` 图标背景颜色



### Example

```html
<avatar src="http://vuejs.org/images/logo.png"></avatar>
<avatar icon="fa fa-user" radius="4px" bg-color="#eee"></avatar>
```
