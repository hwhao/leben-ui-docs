# XLabel

> 行内标签组件



### Props

* icon `String:''` 图标class，与`src`二选一
* src `String:''` 图片地址，与`icon`二选一
* type `String:''` 样式类型，支持`（默认）`、`primary（蓝）`、`success（绿）`、`warning（黄）`、`danger（红）`
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`
* clickable: `Boolean:false` 是否有点击样式


### Example

```html
<x-label>Test</x-label>
<x-label icon="fa fa-envelope">23</x-label>
<x-label src="http://semantic-ui.com/images/avatar/small/elliot.jpg">Elliot</x-label>
```
