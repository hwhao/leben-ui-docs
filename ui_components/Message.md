# Message

> 消息布局组件



### Props

* title `String:''` 标题
* icon `String:''` 图标class
* show `Boolean:true` 设置是否显示或隐藏
* closable `Boolean:false` 是否显示关闭按钮



### Example

```html
<message>内容</message>
<message title="标题">内容</message>
<message title="可关闭的" closeable>点击右侧按钮关闭</message>
<message icon="fa fa-info-circle" title="有图标的">内容</message>
<message title="步骤">
  <ul>
    <li>第一步</li>
    <li>第二步</li>
    <li>第三步</li>
    <li>第四步</li>
    <li>第五步</li>
  </ul>
</message>
```
