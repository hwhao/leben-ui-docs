# Breadcrumb

> 导航条组件



### Props

* items `Array:[]` 导航条数据，支持格式`[String,String,...]`或`[{title:String,link:Object}]`，其中link为v-link所支持的对象
* divider `String:' / '` 分割字符，与dividerIcon二选一
* dividerIcon `String:''` 分割图标，设置此属性后divider将失效
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Event

* on-action `fn(item,index)` 点击子项触发，`item`为当前点击项，`index`为点击的项数



### Example

```html
<breadcrumb :items="items1"></breadcrumb>
<breadcrumb :items="items2" divider-icon="fa fa-angle-right"></breadcrumb>
```
```javascript
data () {
  return {
    items1: ['Home', 'Form', 'XInput'],
    items2: [{
      title: 'Home',
      link: {name: 'home'}
    }, {
      title: 'XInput',
      link: {name: 'x-input'}
    }, {
      title: 'Introduction'
    }]
  }
}
```
