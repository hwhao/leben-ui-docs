# Popup [Directive]

> 悬浮提示泡，基于directive实现



### Props

* popupTitle `String:''` 提示标题，可选
* popupSize `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`


### Example

```html
<x-button v-popup="'我是内容~~~'">无标题</x-button><br>
<x-button v-popup="'我是内容~~~~~~~~~'" popup-title="我是标题">有标题</x-button><br>
<x-input placeholder="用户名" v-popup="'英文字母、阿拉伯数字、下划线'"></x-input><br>

<span v-popup="'popup'" popup-size="small">小</span>
<span v-popup="'popup'">默认</span>
<span v-popup="'popup'" popup-size="large">大</span>
```
