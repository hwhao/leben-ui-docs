# Tabs

> 选项卡组件



### Props

* items `Array:[]` 选项卡列表，`[{title:String,icon:String,link:Object},...]`，`title`选项标题，`icon`为选项图标（可选），`link`为v-link支持的对象（可选）
* type `String,''` 选项卡类型，支持`（默认）`、`underlined（下划线）`、`outlined（外框）`样式
* value `Object:items[0]` 当前选择的选项，默认为选项列表下标为0的选项
* flexed `Boolean:false` 是否使用`display:flex`布局，`flex`可使选项卡占满剩余空间
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Event

* on-action `fn(item,index)` 若选项不存在`link`属性，则点击选项卡时触发该事件，`item`为当前点击的项，`index`为点击项的下标



### Example

```html
<tabs :items="items" @on-action="onClick"></tabs>
```
```javascript
data () {
  return {
    items: [
      {title: 'github', icon: 'fa fa-github'},
      {title: 'qq', icon: 'fa fa-qq'},
      {title: 'weixin', icon: 'fa fa-weixin'},
      {title: 'weibo', icon: 'fa fa-weibo'},
      {title: 'no icon'}
    ]
  }
},
methods: {
  onClick (item, index) {
    console.log('%o\n%o', item, index)
  }
}
```
