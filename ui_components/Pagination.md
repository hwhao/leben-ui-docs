# Pagination

> 分页页码组件



### Props

* current `Number:1` 当前页码
* total `Number:1` 总页码数
* range `Number:3` 可选页码范围
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Event

* on-turn-page `fn(page,current,total)` 点击页码翻页时触发



### Example

```html
<pagination :current="current1" :total="10" @on-turn-page="change1"></pagination>
<pagination :current="current2" :total="50" :range="5" @on-turn-page="change2"></pagination>
```
```javascript
data () {
  return {
    current1: 1,
    current2: 1
  }
},
methods: {
  change1 (i) { this.current1 = i },
  change2 (i) { this.current2 = i }
}
```
