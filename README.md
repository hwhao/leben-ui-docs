# README

LebenUI是一个Vue.js组件库，适用于PC端的Web app。

项目仓库地址：[点我](http://git.oschina.net/hwhao/leben-ui)

项目文档地址：[点我](http://git.oschina.net/hwhao/leben-ui-docs)

[点击查看详细说明](http://git.oschina.net/hwhao/leben-ui-docs/blob/master/SUMMARY.md)

# Install

### 安装
```bash
# 安装leben-ui
$ npm install leben-ui --save
```

### 使用

1. FontAwesome：LebenUI使用了FontAwesome中的部分图标，并且也推荐用户使用`<i class="fa fa-xxx"></i>`的方式添加图标。请在index.html的head中添加以下cdn：
```html
<link href="//cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
```

2. loaders.css（可选）：LebenUI中的`Loader`组件样式依赖于`loaders.css`，如果使用到该组件，请引入：
```html
<link href="//cdn.bootcss.com/loaders.css/0.1.2/loaders.min.css" rel="stylesheet">
```

3. vue-router、vue-resource：LebenUI中的`SideMenu`组件使用了vue-router进行视图跳转，`ImgUploader`组件使用了vue-resource进行异步提交，如果使用了这些组件，请自行添加依赖。
```bash
# 安装vue-router
$ npm install vue-router --save
$ npm install vue-resource --save
```

4. LebenUI：

  方法一：Vue插件形式引用

  ```javascript
  import LebenUI from 'leben-ui/dist/leben-ui'
  import 'leben-ui/dist/leben-ui.css'

  Vue.use(LebenUI)
  ```

  方法二：单个组件形式引用
  ```javascript
  import 'leben-ui/dist/leben-ui.css'
  import SideMenu from 'leben-ui/dist/components/SideMenu'
  import XButton from 'leben-ui/dist/components/XButton'
  import Popup from 'leben-ui/dist/directives/Popup'
  ...
  Vue.component('SideMenu', SideMenu)
  Vue.component('XButton', XButton)
  Vue.directive('Popup', Popup)
  ...
  ```

### 全局样式

> 所有leben-ui全局样式均以`ui-`为前缀

***浮动：***

  `ui-fl` 左浮动，`ui-fr` 右浮动，`ui-cf` 清除浮动

***边距：***

  组成：`ui`-`边距名称`-`边距尺寸`

  边距名称：`mg（外边距）`，`pd（内边距）`，`mt（上内外边距）`，`mr（右外边距）`，`mb`，`ml`，`pt`，`pr`，`pb`，`pl`

  边距尺寸：`xxs`，`xs`，`s`，`l`，`xl`，`xxl`

  例如：`ui-mg-s`，`ui-pr-xl`，……
