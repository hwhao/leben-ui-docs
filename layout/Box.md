# Box

> 盒子Div组件，支持header和footer



### Props

* title `String:''` 标题（可选）
* icon `String:''` 图标class（可选）
* type `String:''` 边框样式，可选`normal`、`primary`、`success`、`warning`、`danger`
* basic `Boolean:false` 是否为基础样式（去除边框、阴影）



### Example

```html
<box style="width:300px;">
  <div>
    box<br><br><br><br><br><br>
  </div>
</box>

<box basic style="width:300px;">
  <div>
    basic box<br><br><br><br><br><br>
  </div>
</box>

<box style="width:300px;" type="primary">
  <div>
    basic<br><br><br><br><br><br>
  </div>
</box>

<box style="width:300px;">
  <div slot="header">
    Custom Header
  </div>
  <div>
    header & footer<br><br><br><br><br><br>
  </div>
  <div slot="footer">
    Custom Footer
  </div>
</box>

<box style="width:300px;">
  <div slot="header">
    <tabs :items="[{title:'tab1'},{title:'tab2'}]" type="underlined"></tabs>
  </div>
  <div>
    Tabs<br><br><br><br><br><br>
  <div>
</box>
```
