# SideMenu

> 侧边二级菜单，依赖于vue-router
> 配合div.ui-wrapper使用，或为其父级容器添加padding:240px



### Props

* icon `String:''` 标题栏图标class
* title `String:''` 标题栏文本
* menus `Array:[]` 菜单结构，支持二级菜单，需要vue-router支持，详细使用方法请查看例子，其中`title`为菜单中显示的标题，`icon`为图标class；`name`为vue-router配置的name；`group`为路由组，当vue-router的路由配置了相同的group参数，每当跳转到这些路由时，虽然`name`可能不匹配，但也会激活该菜单样式



### Example

```html
<side-menu :menus="menus">
  <i slot="icon" class="fa fa-pie-chart"></i>
  <span slot="title">title</span>
  <div slot="footer" style="padding:20px;">
    footer
  </div>
</side-menu>
```
```javascript
export default {
  data () {
    return {
      menus: [
        // 一级菜单：
        {title: 'Home', icon: 'fa fa-home', name: 'home'},
        // 两级菜单：
        {
          title: 'Form',
          icon: 'fa fa-edit',
          // 子菜单：
          submenus: [
            {title: 'Input', name: 'input'},
            {title: 'Radio', name: 'radio'},
            {title: 'Checkbox', name: 'checkbox'}
          ]
        }
      ]
    }
  }
}
```
***重要：***
```javascript
router.beforeEach(({to, next}) => {
  router.app.$broadcast('viewChange', to.name, to.group)
  next()
})
```
