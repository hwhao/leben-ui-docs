# SidePanel

> 侧边面板组件



### Props

* type `String:'right'` 面板位置，支持`right（默认）`、`left`
* show `Boolean:false` 控制面板显示隐藏
* closeable `Boolean:false` 是否显示收起按钮
* padding `String:'20px'` 内边距，为`0`时，去除内边距


### Event

* on-show `fn()` 面板显示时触发
* on-close `fn()` 面板关闭时触发



### Example

```html
<x-button type="primary" @click="show1=!show1">右侧面板（有关闭按钮）</x-button>
<x-button type="primary" @click="show2=!show2">右侧面板（无关闭按钮）</x-button>
<x-button type="primary" @click="show3=!show3">左侧面板</x-button>
<side-panel :show.sync="show1" closeable>
  <div style="width:400px;">
    <h3>右侧面板（有关闭按钮）</h3>
  </div>
</side-panel>
<side-panel :show.sync="show2">
  <div style="width:300px;">
    <h3>右侧面板（无关闭按钮）</h3>
  </div>
</side-panel>
<side-panel :show.sync="show3" type="left" closeable>
  <div style="width:400px;">
    <h3>左侧面板</h3>
  </div>
</side-panel>
```
```javascript
data () {
  return {
    show1: false,
    show2: false,
    show3: false
  }
}
```
