# ImgUploader

> 图片上传组件



### Props

* url `String:''` 图片删除地址
* submitText `String:'上传'` 上传按钮文本



### Event

* on-empty `fn()` 当图片列表为空时上传触发
* on-response `fn(res)` 返回图片上传结果，`res`为返回数据，`done`为回调函数，执行后结束上传动画，`clear`为回调函数，执行后清空图片列表



### Example

```html
<img-uploader submit-text="上传头像" url="http://localhost:8888/upload" @on-response="handleRes" @on-empty="empty"></img-uploader>
```
```javascript
methods: {
  handleRes (res, done, clear) {
    console.log(res)
    done()
    clear()
  },
  empty () {
    console.log('no files')
  }
}
```
