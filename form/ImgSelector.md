# ImgSelector

> 图片选择组件，仅提供选择文件和显示缩略图功能，图片上传功能请使用ImgUploader



### Props

* maxLength `Number:5` 最大可选图片数
* photos `Array` 图片文件列表，`[{file,imgBase64}]`包含文件信息file（可用于上传），和图片base64数据



### Event

* on-add `fn(photo)` 每添加一张图片时触发，photo为该图片信息（文件、base64）
* on-remove `fn(photo)` 每移除一张图片时触发，photo为该图片信息（文件、base64）



### Example

```html
<img-selector :photos="photos" :max-length="10" @on-add="add" @on-remove="remove"></img-selector>
```
```javascript
data () {
  return {
    photos: []
  }
},
methods: {
  add (photo) {
    console.log(photo)
  },
  remove (photo) {
    console.log(photo)
  }
}
```
