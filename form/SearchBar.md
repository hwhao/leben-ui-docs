# SearchBar

> 搜索输入框组件，支持异步搜索动画



### Props

* value `String:''` 搜索输入框值
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`
* placeholder `String` 提示文本
* debounce `Number:0` 输入延迟，配合immediate使用
* immediate `Boolean:false` 输入值改变是否立即调用搜索
* radius `String:'100px'` 输入框圆角大小
* loading `Boolean:false` 是否显示加载动画



### Event

* on-search `fn(done,value)` 点击搜索按钮或自动调用搜索时触发，`done`为函数，调用后完成动画，`value`为输入框的值
* on-focus `fn(value)` 当输入框获得焦点时触发，`value`为输入框的值
* on-blur `fn(value)` 当输入框获失去焦点时触发，`value`为输入框的值



### Example

```html
<search-bar placeholder="搜索" @on-search="search"></search-bar>
```
```javascript
methods: {
  search (done, value) {
    console.log('searching: ' + value)
    // setTimeout模拟异步处理
    setTimeout(() => {
      done()
    }, 2000)
  }
}
```
