# Radio

> 单选按钮组件



### Props

* value `String` 表单值
* option `String` 当前组件选项值
* checked `Boolean:false` 是否选中
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Example

```html
<radio option="ios" :value.sync="group1" :checked.sync="checked1">IOS</radio>
<radio option="android" :value.sync="group1" :checked.sync="checked2">Android</radio>
<div>
  <p>ios: {{checked1|json}}</p>
  <p>android: {{checked2|json}}</p>
  <p>result: {{group1}}</p>
</div>
```
```javascript
data () {
  return {
    group1: 'ios',
    checked1: null,
    checked2: null
  }
}
```
