# Dropdown

> 下拉框组件



### Props

* options `Array` 选项列表，格式必须为`[{title:String,value:Object}]`
* default `Object:null` 默认选中的选项
* value `Object:null` 当前选中的选项
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Example

```html
<dropdown :options="options" :default="options[0]" :value.sync="value"></dropdown>
<div>{{value|json}}</div>
```
```javascript
data () {
  return {
    options: [{
      title: '男',
      value: 'male'
    }, {
      title: '女',
      value: 'famale'
    }],
    value: null
  }
}
```
