# XInput

> input组件



### Props

* name `String` 表单组件名称，用于XForm表单验证
* value `String` 表单值
* placeholder `String` 提示文字
* type `String:'text'` 类型，可支持`text`、`password`
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`
* debounce `Number:0` 响应延迟（毫秒）

#### 验证属性（配合XForm使用）

* required `Boolean:false` 是否必填
* email `Boolean:false` 是否验证电子邮箱格式
* phone `Boolean:false` 是否验证手机号码格式
* equal `[String,Boolean]:false` 是否等值，绑定在另一个变量上，可用于判断两次输入密码是否相同的案例上
* validators `Object:null` 自定义验证器



### Example

***type***
```html
<x-input :value.sync="text"></x-input>
<x-input placeholder="type=text" :value.sync="text"></x-input>
<x-input placeholder="type=password" type="password" :value.sync="text"></x-input>
```

***size***
```html
<x-input placeholder="small" size="small"></x-input>
<x-input placeholder="normal"></x-input>
<x-input placeholder="large" size="large"></x-input>
```

***x-form***
```html
<x-form name="form1">
  <x-input placeholder="Username" name="username" required></x-input>
  <x-input placeholder="Email" name="email" email></x-input>
  <x-input placeholder="Phone" name="phone" phone></x-input>
  <div>
    <p style="line-height:.5em;" v-for="(key,item) in $form1">{{key}}: {{item|json}}</p>
    <p v-show="!$form1.username.valid&&$form1.username.dirty">用户名不能为空</p>
    <p v-show="!$form1.email.valid&&$form1.email.dirty">邮箱地址格式不正确。</p>
    <p v-show="!$form1.phone.valid&&$form1.phone.dirty">手机号码格式不正确。</p>
  </div>
  <x-button @click="submit">提交</x-button>
</x-form>
```

***equal验证***
```html
<x-input placeholder="Password" :value.sync="p1"></x-input>
<x-input placeholder="Confirm Password" :value.sync="p2" :equal.sync="p1"></x-input>
```

***自定义验证器***
```html
<x-input placeholder="Custom1" value.sync="val1" :validators="validators"></x-input>
<x-input placeholder="Custom2" value.sync="val2" :validators="{word:word}"></x-input>
```
```javascript
data () {
  return {
    val1: '',
    val2: '',
    validators: {
      empty (val) { return val.indexOf(' ') === -1 },
      limit (val) { return val.length < 10 }
    }
  }
},
methods: {
  word (val) {
    return /^[0-9a-zA-Z_]+$/.test(val)
  }
}
```
