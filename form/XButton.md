# XButton

> 按钮组件



### Props

* type `String:''` 按钮样式类型，支持`（默认）`、`primary（蓝）`、`success（绿）`、`warning（黄）`、`danger（红）`
* basic `Boolean:false` 基础按钮样式（显示边框，无背景颜色）
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`
* icon `String:''` 按钮图标class
* loading `Boolean:false` 是否显示进度动画，配合@on-action事件使用
* disabled `Boolean:false` 禁用按钮，将会禁用click和@on-action事件



### Event

* on-action `fn(done)` 点击按钮时触发，并显示进度动画，`done`为回调函数，执行后结束进度动画



### Example

```html
<x-button @click="click">normal</x-button>
<x-button type="primary" icon="fa fa-edit" @click="click">primary</x-button>
<x-button type="success" icon="fa fa-check" @click="click">info</x-button>
<x-button type="warning" icon="fa fa-warning" @click="click">warning</x-button>
<x-button type="danger" icon="fa fa-close" @click="click">danger</x-button>
```

***basic***
```html
<x-button @click="click" basic>normal</x-button>
<x-button type="primary" basic icon="fa fa-edit" @click="click">primary</x-button>
<x-button type="success" basic icon="fa fa-check" @click="click">info</x-button>
<x-button type="warning" basic icon="fa fa-warning" @click="click">warning</x-button>
<x-button type="danger" basic icon="fa fa-close" @click="click">danger</x-button>
```

***loading***
```html
<x-button icon="fa fa-check" basic loading @on-action="load">click</x-button>
<x-button icon="fa fa-check" loading @on-action="load">click</x-button>
<x-button loading @on-action="load">no icon</x-button>
```
```javascript
methods: {
  load (done) {
    console.log('click')
    setTimeout(() => {
      console.log('done')
      done()
    }, 1000)
  }
}
```

***disabled***
```html
<x-button type="primary" basic icon="fa fa-check" disabled @click="click">click</x-button>
<x-button type="primary" icon="fa fa-check" disabled @click="click">click</x-button>
<x-button type="danger" icon="fa fa-check" disabled loading @on-action="load">click</x-button>
```
