# Checkbox

> 多选按钮组件



### Props

* value `String` 表单值
* checked `Boolean:false` 是否选中
* model `Array` 绑定到数组，可用于获取所有选中的值
* size `String:''` 尺寸，可支持`（默认：中）`、`small（小）`、`large（大）`



### Event

* on-change `fn(checked,val)` 选中状态改变时触发



### Example

```html
<checkbox>是否同意该条例</checkbox>
```

***绑定到数组***
```html
<example>
  <checkbox v-for="option in options" :value="option.value" :model="selectedArr" @on-change="check">{{option.text}}</checkbox>
</example>
<p>{{selectedArr|json}}</p>
```
```javascript
data () {
  return {
    options: [
      { checked: false, value: 'apple', text: '苹果' },
      { checked: false, value: 'banana', text: '香蕉' },
      { checked: false, value: 'watermelon', text: '西瓜' }
    ],
    selectedArr: []
  }
}
```
